/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author robert
 */
package com.example.vaadin;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author robert
 */
@org.springframework.stereotype.Controller    // This means that this class is a Controller
@RequestMapping(path="/demo")
public class Controller {
    @Autowired
    private BookRepository bookrepository;
    
    @Autowired
    private BookCategoryRepository catrepositry;
    
    @GetMapping(path="/findall")
    public @ResponseBody Iterable<BookCategory> getAllCat() {
		// This returns a JSON or XML with the users
	return catrepositry.findAll();
    }
    
    @GetMapping(path="/find")
    public @ResponseBody Iterable<Book> findUsersByName(@RequestParam String name){
        return bookrepository.findByName(name);
        
    }
    
    @GetMapping(path="/findbookbycat")
    public @ResponseBody Iterable<Book> findBookByBookCat(@RequestParam String name){
        BookCategory cat = catrepositry.findByName(name);
        return bookrepository.findByBookCategory(cat);
        
    }
    
    @GetMapping(path="/all")
    public @ResponseBody Iterable<Book> getAllUsers() {
		// This returns a JSON or XML with the users
	return bookrepository.findAll();
    }
        
    @GetMapping(path="/addcat")    
        public @ResponseBody String addNewCat(@RequestParam String name ){
            BookCategory c = new BookCategory(name);
            catrepositry.save(c);
            return "Saved Category";
        }
        
    @GetMapping(path="/deletebook") // Map ONLY GET Requests
	public @ResponseBody String deleteBook (@RequestParam String name) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
		bookrepository.deleteByName(name);
		return "Delate book";
	}

        
    @GetMapping(path="/addbook") // Map ONLY GET Requests
	public @ResponseBody String addNewBook (@RequestParam String name, @RequestParam String cat) {
                BookCategory b = null;
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
                if (catrepositry.findByName(cat)!=null){
                b = catrepositry.findByName(cat);
                }
                else{
                b = new BookCategory(cat);
                catrepositry.save(b);
                }
                
                Book n = new Book(name,b);
		bookrepository.save(n);
		return "Saved book";
	}
}
