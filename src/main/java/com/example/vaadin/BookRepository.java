/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vaadin;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author robert
 */
public interface BookRepository extends JpaRepository<Book, Long> {
    public List<Book> findByName(String email);
    @Transactional
    public void deleteByName(String name);
    public List<Book> findByBookCategory(BookCategory bookCategory);
}
